import random
import time
from difflib import SequenceMatcher

# Manage Database _________________________________________________


def reset_active(con):
    con.execute('UPDATE motives SET active = FALSE WHERE active IS TRUE')
    con.commit()


def reset_daily_scores(con):
    con.execute('UPDATE user SET daily_score = 0')
    con.commit()


def similar(a, b):
    return SequenceMatcher(None, a, b).ratio()

# Getter __________________________________________________________


def get_active(con):
    active_id = con.execute('SELECT id_motive FROM motives WHERE active IS TRUE').fetchone()
    try:
        return active_id[0]
    except TypeError:
        logging("Fehler, keine aktiven Motive gefunden.")


def get_motive(con):
    with con:
        random.seed(time.time())
        ids = con.execute('SELECT id_motive FROM motives WHERE used IS NULL').fetchall()
        ids = random.choice(ids)[0]
        con.execute('UPDATE motives SET used = (?) WHERE id_motive = (?)', ("True", ids))
        con.commit()
        return [ids, con.execute('SELECT motive FROM motives WHERE id_motive = (?)', (ids,)).fetchone()]


def get_active_motive(con, motive_id):
    tmp = con.execute('SELECT motive FROM motives WHERE id_motive = (?)', (motive_id,)).fetchone()
    return tmp[0]


def get_user(con):
    with con:
        return con.execute('SELECT id_user FROM user ORDER BY score DESC').fetchall()


def get_user_score(con, id_user):
    with con:
        score = con.execute('SELECT score FROM user WHERE id_user = (?)', (id_user,)).fetchone()
        return score[0]


def get_daily_user_score(con, id_user):
    with con:
        score = con.execute('SELECT daily_score FROM user WHERE id_user = (?)', (id_user,)).fetchone()
        try:
            if score[0] > 0:
                return score[0]-1
            else:
                return 0
        except TypeError:
            return 0


def get_images(con, motive_id):
    with con:
        picture_list = con.execute('SELECT file_name FROM pictures INNER JOIN motives ON motives.id_motive = pictures.id_motive WHERE used IS NOT NULL AND motives.id_motive = (?)', (motive_id,)).fetchall()
        return picture_list


def get_images_user(con, user_id):
    with con:
        picture_list = con.execute('SELECT file_name FROM pictures WHERE pictures.id_user = (?)', (user_id,)).fetchall()
        return picture_list


def get_motive_ids(con):
    with con:
        id_list_motives = con.execute('SELECT id_motive FROM motives WHERE used IS NOT NULL').fetchall()
        id_list_pictures = con.execute('SELECT id_motive FROM pictures').fetchall()
        return list(set(id_list_pictures).intersection(id_list_motives))


def get_motive_name(con, movie_id):
    with con:
        text = con.execute('SELECT motive FROM motives WHERE id_motive = (?)', (movie_id,)).fetchone()
        return text[0]

# Setter __________________________________________________________


def set_active(con, id_motive):
    con.execute('UPDATE motives SET active = TRUE WHERE id_motive = (?)', (id_motive,))
    con.commit()


def set_motive(con, text, added_by):
    all_motives = con.execute("SELECT motive FROM motives").fetchall()
    similarity_score = 0

    for i in all_motives:
        tmp = similar(i[0], text)
        if tmp > similarity_score:
            similarity_score = tmp

    if similarity_score >= 0.8:
        main.logging("Score: " + similarity_score)
        return False
    else:
        with con:
            con.execute('INSERT INTO motives (motive, added_by) VALUES (?, ?)', (text, added_by))
            con.commit()
        return True


def set_picture(con, id_motive, id_user, file_name):
    with con:
        if con.execute('SELECT 1 FROM pictures WHERE file_name = (?)', (file_name,)).fetchone():
            return False
        else:
            con.execute('INSERT INTO pictures (id_motive, id_user, file_name) VALUES (?, ?, ?)', (id_motive, id_user, file_name))
            con.commit()
            return True


def set_user_score(con, id_user):
    with con:
        if not con.execute('SELECT 1 FROM user WHERE id_user = (?)', (id_user,)).fetchone():
            con.execute('INSERT INTO user (id_user, score) VALUES (?,?)', (id_user, 1))
            con.commit()
        elif con.execute('SELECT 1 FROM user WHERE id_user = (?) AND score IS NOT NULL', (id_user,)):
            score = con.execute('SELECT score FROM user WHERE id_user = (?)', (id_user,)).fetchone()
            score = int(score[0]) + 1
            con.execute('UPDATE user SET score = (?) WHERE id_user = (?)', (score, id_user))
            con.commit()


def set_daily_user_score(con, id_user, score):
    with con:
        con.execute('UPDATE user SET daily_score = (?) WHERE id_user = (?)', (score, id_user))
        con.commit()
