import discord
from PIL import Image
import piexif
from datetime import datetime
import secret
import sqlite3 as sl
import db_access
import os
from discord.ext import tasks

debug = False


# Logging __________________________________________________#

def logging(text):
    f = open(log_file, "a")
    tmp = str(datetime.now()) + " " + text
    print(tmp)
    tmp += "\n"
    f.write(tmp)
    f.close()


# Vorbereitung Client_______________________________________#

intents = discord.Intents.default()
intents.members = True
intents.presences = True
client = discord.Client(intents=intents)


@client.event
async def on_ready():
    logging("\n\n" + "Logged in as\n" + str(client.user.name) + "\n" + str(client.user.id) + "\n")


# Vorbereitungen ___________________________________________#


if debug:
    con = sl.connect('challenge_debug.sqlite')
    folder_name = "pictures_debug"
    channel_themen = 888391057237225542
    channel_einreichen = 888391057237225542
    channel_archiv = 888391057237225542
    channel_austausch = 888391057237225542
    log_file = "log_debug.txt"
    logging("Programmneustart ____________________________________")
    logging("Debug-Modus aktiviert.")
if not debug:
    con = sl.connect('challenge.sqlite')
    folder_name = "pictures"
    channel_themen = 831447981215580252
    channel_einreichen = 888390763614978098
    channel_archiv = 889487570566807572
    channel_austausch = 888390836511973407
    log_file = "log.txt"
    logging("Programmneustart ____________________________________")
    logging("Live-Modus aktiviert")

db_access.reset_active(con)


# Vorbereitung Loop_________________________________________#

@tasks.loop(minutes=2880)
async def motive_loop():
    await client.wait_until_ready()

    # Aufräumen von einreichen
    channel = client.get_channel(channel_einreichen)
    await channel.purge(limit=None, check=lambda msg: not msg.pinned)

    # Aufräumen von themen
    channel = client.get_channel(channel_themen)
    await channel.purge(limit=None, check=lambda msg: not msg.pinned)

    # Zurücksetzen des aktiven Themas und Daily Score
    db_access.reset_active(con)

    # Updaten des Daily Scores
    for user in db_access.get_user(con):
        score = db_access.get_daily_user_score(con, user[0])
        if score >= 3:
            db_access.set_user_score(con, user[0])
            db_access.set_user_score(con, user[0])
            logging(f"{user[0]} hat 2 Extrapunkte bekommen")
            break
        if score >= 2:
            db_access.set_user_score(con, user[0])
            logging(f"{user[0]} hat 1 Extrapunkt bekommen")
            break
    db_access.reset_daily_scores(con)

    try:
        motive_raw = db_access.get_motive(con)
        motive_id = motive_raw[0]
        motive_name = motive_raw[1]
        db_access.set_active(con, motive_id)
        await channel.send(f"@everyone Das heutige Motiv lautet `{motive_name[0]}` und hat die ID {motive_id}. Reiche dein Bild mit dem Text `!add {motive_id}` ein.")
        logging(f"Es wurde das Motiv {motive_name[0]} mit der ID {motive_id} ausgewählt.")
    except IndexError:
        await channel.send("Ich habe in der Datenbank keine Wörter mehr. Füge mehr mit `!motiv <wort>` hinzu.")
        logging("Die Datenbank hat keine Wörter mehr.")


@client.event
async def on_raw_reaction_add(payload):
    if payload.channel_id == channel_einreichen:
        if payload.emoji.name == 'up':
            channel = client.get_channel(channel_einreichen)
            message = await channel.fetch_message(payload.message_id)
            id_user = message.author.id
            reaction = discord.utils.get(message.reactions, emoji=payload.emoji)
            db_access.set_daily_user_score(con, id_user, reaction.count)


@client.event
async def on_message(message):
    # Abfangen Befehle Themen___________________________________#

    if message.channel == channel_themen:
        if message.author.id != 825662384361177120:
            user_id = message.author.id
            await message.delete()

    # Abfangen Befehle einreichen_______________________________#

    channel = client.get_channel(channel_einreichen)

    if message.channel == channel:
        if message.content.startswith('!motiv'):
            text = message.content
            motive = text.partition(' ')[2]
            user_id = message.author.id
            if db_access.set_motive(con, motive, user_id):
                await message.channel.send("Ich habe das Wort hinzugefügt.")
                logging(f"{user_id} hat das Wort {motive} hinzugefügt.")
            else:
                await message.channel.send("Das Wort gibt es schon!")
                logging(f"{user_id} hat versucht das Wort {motive} hinzuzufügen, war schon vorhanden.")

        elif message.content.startswith('!add'):
            text = message.content
            motive_id = text.partition(' ')[2]
            user_id = message.author.id

            if int(motive_id) is int(db_access.get_active(con)):
                image_types = ["jpg"]
                for attachment in message.attachments:
                    if any(attachment.filename.lower().endswith(image) for image in image_types):
                        filename = f"{user_id}_{motive_id}.jpg"
                        await attachment.save(filename)
                        os.replace(filename, f"{folder_name}/{filename}")
                        image = Image.open(f"{folder_name}/{filename}")

                        if "exif" in image.info:
                            exif_dict = piexif.load(image.info["exif"])

                            if piexif.ImageIFD.Orientation in exif_dict["0th"]:
                                orientation = exif_dict["0th"].pop(piexif.ImageIFD.Orientation)
                                exif_bytes = piexif.dump(exif_dict)

                                if orientation == 2:
                                    image = image.transpose(Image.FLIP_LEFT_RIGHT)
                                elif orientation == 3:
                                    image = image.rotate(180)
                                elif orientation == 4:
                                    image = image.rotate(180).transpose(Image.FLIP_LEFT_RIGHT)
                                elif orientation == 5:
                                    image = image.rotate(-90, expand=True).transpose(Image.FLIP_LEFT_RIGHT)
                                elif orientation == 6:
                                    image = image.rotate(-90, expand=True)
                                elif orientation == 7:
                                    image = image.rotate(90, expand=True).transpose(Image.FLIP_LEFT_RIGHT)
                                elif orientation == 8:
                                    image = image.rotate(90, expand=True)

                        image.thumbnail((1920, 1080), Image.ANTIALIAS)
                        image.save(f"{folder_name}/{filename}", "JPEG")
                        tmp = db_access.set_picture(con, motive_id, user_id, filename)
                        if tmp is False:
                            text = f"<@{user_id}> Du hast bereits ein Bilder unter der ID {motive_id} hochgeladen, dein altes Bild wurde ersetzt"
                            logging(f"{user_id} hat sein Bild mit der ID {motive_id} geupdatet.")
                            await message.add_reaction(emoji="up:888830083635216425")
                            await message.channel.send(text)
                        elif tmp is True:
                            text = f"<@{user_id}> Bild eingereicht. Danke!"
                            logging(f"{user_id} hat ein Bild mit der ID {motive_id} eingereicht. Dateiname ist {filename}")
                            await message.add_reaction(emoji="up:888830083635216425")
                            await message.channel.send(text)
                            db_access.set_user_score(con, user_id)
                    else:
                        await message.delete()
                        await message.channel.send(f"<@{user_id}> Bitte lade deine Datei im .jpg Format hoch!")
                        logging(f"{user_id} hat das falsche Format gewählt.")
            else:
                await message.delete()
                active_id = db_access.get_active(con)
                await message.channel.send(f"<@{user_id}> die ID {motive_id} ist nicht aktiv. Aktuell ist das thema {db_access.get_active_motive(con, active_id)} mit der ID {active_id} aktiv.")

        else:
            if message.author.id != 825662384361177120:
                logging("Falscher Befehlt.")
                await message.delete()

    # Abfangen Befehle archiv____________________________________#

    channel = client.get_channel(channel_archiv)

    if message.channel == channel:
        if message.content.startswith('!show'):
            await client.get_channel(channel_archiv).purge(limit=None, check=lambda msg: not msg.pinned)
            text = message.content
            split = text.partition(' ')[2]
            if not split.startswith("<"):
                image_list = db_access.get_images(con, split)
                if len(image_list) == 0:
                    await message.channel.send("Bisher wurden noch keine Bilder eingereicht.")
                else:
                    for image in image_list:
                        image = image[0]
                        await message.channel.send(f"Bild von <@{image.split('_')[0]}>", file=discord.File(f"{folder_name}/{image}"))
            else:
                user_id = message.mentions[0].id
                image_list = db_access.get_images_user(con, user_id)
                if len(image_list) == 0:
                    await message.channel.send(f"Bisher wurden noch keine Bilder von <@{user_id}> eingereicht.")
                else:
                    await message.channel.send(f"Bilder von <@{user_id}>")
                    for image in image_list:
                        image = image[0]
                        await message.channel.send(file=discord.File(f"{folder_name}/{image}"))
        else:
            if not debug:
                if message.author.id != 825662384361177120:
                    await message.delete()

    # Abfangen Befehle austausch_________________________________#

    channel = client.get_channel(channel_austausch)

    if message.channel == channel:
        if message.content == '!list':
            list = db_access.get_motive_ids(con)
            list.sort()
            tmp = "Folgende Bilder sind abrufbar:"
            for item in list:
                item = item[0]
                tmp += f"\nID {item} - {db_access.get_motive_name(con, item)}"
            await message.author.send(tmp)

        elif message.content == '!highscore':
            user_ids = db_access.get_user(con)

            if len(user_ids) > 0:
                tmp = ""
                counter = 1
                previous = 0
                for user in user_ids:
                    score = db_access.get_user_score(con, user[0])
                    if score == previous:
                        counter -= 1
                    tmp += f"{counter}. <@{user[0]}> ({score})\n"
                    previous = db_access.get_user_score(con, user[0])
                    counter += 1
                await message.channel.send(tmp)
            else:
                await message.channel.send("Bisher hat noch keiner ein Bild eingereicht, sei der Erste!")

        elif message.content == '!help':
            await message.channel.send(
                "Folgende Befehle stehen zur Verfügung:\n\nChannel <#888390763614978098>\n`!motiv <wort>` um ein neues Motiv hinzuzufügen\n`!add <id>` mit einem Bild zusammen um dieses einzureichen\n\nChannel <#889487570566807572>\n`!show <id>/<user>` um alle Bilder zu dieser ID zu sehen\n\nChannel <#888390836511973407>\n`!list` um eine Liste an bereits vorhandenen Motiven zu bekommen\n`!highscore` um die aktuelle Rangliste zu sehen\n`!help` dieser Befehl")
    # Abfangen Befehle verwalten________________________________#

    if message.content.startswith('!delete'):
        if message.author.id == 250678389399093248:
            await message.channel.purge(limit=None, check=lambda msg: not msg.pinned)
        else:
            logging(f"{message.author} hat versucht den !delete Befehl auszuführen.")
            await message.author.send("`!delete` ist nicht für dich gedacht.")

    if message.content == '!run':
        if message.author.id == 250678389399093248:
            await motive_loop()


motive_loop.start()

client.run(secret.appToken)
